1. use build.sh to build the new jar, you will need to have the presto jar and run "mvn install" already
2. use the run.sh to start presto server with the new jar, you will need to create a config.properties to enable the data sources
3. run a query via presto CLI or REST API
4. visualize the pipeline performance graph in html `http://localhost/pipeline_performance.html`

