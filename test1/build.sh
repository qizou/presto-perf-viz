#! /bin/bash

classpathdir=$1
inputjar=$2
outputjar=$3
classpath="";

aspectdir=$3 #can be the current dir, which takes all of the aj files

usage() {
    echo "Usage: build.sh CLASSPATHDIR INPUTJAR OUTPUTJAR"
    echo "  the CLASSPATHDIR need to have all of the dependent jars, hetu-server/target/hetu-server-316/lib would be a good candidate"
    exit 1;
}



#cp /home/qizou/Downloads/hetu-core/hetu-heuristic-index-bitmap/target/lib/testng-6.10.jar /home/qizou/Downloads/hetu-core/hetu-server/target/hetu-server-1.0.0-SNAPSHOT/lib/
#cp /home/qizou/Downloads/hetu-core/hetu-heuristic-index-hive/target/lib/assertj-core-3.11.1.jar /home/qizou/Downloads/hetu-core/hetu-server/target/hetu-server-1.0.0-SNAPSHOT/lib/

[[ -z "$classpathdir" || -z "$inputjar" || -z "$outputjar" ]] && usage;

build_classpath() {
  for i in `ls $classpathdir/*.jar`; do classpath=$classpath:$i; done
  classpath=$classpath:target/test1-1.0-SNAPSHOT.jar
}

build_classpath;


/usr/bin/ajc -1.9 -cp "/usr/share/java/aspectjrt.jar:$classpath" -inpath $inputjar -outjar $outputjar /home/qizou/Downloads/presto-perf-viz/test1/src/main/java/io/perf/li/PerformanceVisualization.java

