#! /bin/bash

classpathdir=$1
inputjar=$2
classpath=$inputjar;

class=io.prestosql.server.PrestoServer

aspectdir=$3 #can be the current dir, which takes all of the aj files

usage() {
    echo "Usage: run.sh LIB_DIR JAR MAIN_CLASS"
    echo "  the CLASSPATHDIR need to have all of the dependent jars, hetu-server/target/hetu-server-316/lib would be a good candidate"
    exit 1;
}

[[ -z "$classpathdir" || -z "$inputjar" ]] && usage;

build_classpath() {
  for i in `ls $classpathdir/*.jar`; do classpath=$classpath:$i; done
  echo $classpath
}

build_classpath;

#java -ea -XX:+UseG1GC -XX:G1HeapRegionSize=32M -XX:+UseGCOverheadLimit -XX:+ExplicitGCInvokesConcurrent -Xmx20G -Dconfig=hetu-server-1.0.0-SNAPSHOT/etc/config.properties -Dlog.levels-file=hetu-server-316/etc/log.properties -Djdk.attach.allowAttachSelf=true -cp "aspectjrt.jar:$classpath" $class;
java -ea -XX:+UseG1GC -XX:G1HeapRegionSize=32M -XX:+UseGCOverheadLimit -XX:+ExplicitGCInvokesConcurrent -Xmx220G -Dconfig=/opt/perf-viz/hetu-server-1.0.0-SNAPSHOT/etc/config.properties -Dlog.levels-file=/opt/perf-viz/hetu-server-1.0.0-SNAPSHOT/etc/log.properties -Djdk.attach.allowAttachSelf=true -cp "/opt/perf-viz/aspectjrt.jar:$classpath" $class;
