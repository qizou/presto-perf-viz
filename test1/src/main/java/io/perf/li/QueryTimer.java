package io.perf.li;

import io.airlift.units.Duration;
import io.prestosql.operator.Driver;
import io.prestosql.operator.Operator;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import org.aspectj.lang.annotation.Pointcut;
import org.joda.time.DateTime;
import org.joda.time.Period;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * a tool that would allow capturing specific type of operators based on name
 *
 * a map keep track of the time spent on each operator, each stage, each pipeline.
 * each invocation of a method will be kept track off, each operator will have a
 * - earliest start time
 * - each invocation start time
 * - each invocation end time
 * - latest end time;
 * all operators are link to the same query id
 */
@Aspect
public class QueryTimer {
    @Pointcut("call(* io.prestosql.execution.QueryManagerStats.*(..)) && target(io.prestosql.execution.QueryManagerStats)")
    public void queryend() {
    }

    @Around("queryend()")
    public Object dumptoDisk(ProceedingJoinPoint jp) throws Throwable {
        //dump the stats to disk
//        System.out.println("======== dumping stats to disk ============");
        return jp.proceed();
    }

    public void writeToFile(String output, boolean append) throws IOException {
        BufferedWriter out = null;

        try {
            FileWriter fstream = new FileWriter("out.txt", append);
            out = new BufferedWriter(fstream);
            out.write(output + "\n");
        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    @Pointcut("call(* io.prestosql.server.PrestoServer.*(..)) && target(io.prestosql.server.PrestoServer)")
    public void serverStart() {
    }

    @Around("serverStart()")
    public Object aroundStart(ProceedingJoinPoint jp)
            throws Throwable
    {
        String name = jp.getSignature().getName();
        if (name.contains("run")) {
            System.out.println("  ======================== AOP SERVER STARTING ====================");
            Thread.sleep(4000);
            Object result = jp.proceed();
            return result;
        }
        writeToFile("operator,start,duration", false);
        return jp.proceed();
    }

    @Pointcut("call(* io.prestosql.operator.Driver.*(..)) && target(io.prestosql.operator.Driver)")
    public void driver() {
    }

    @Around("driver()")
    public Object aroundDriver(ProceedingJoinPoint jp)
            throws Throwable {
        String name = jp.getSignature().getName();
        if (name.contains("process")) {
            long start = System.nanoTime();

            // timer start
            Object result = jp.proceed();
            // result finished, record time
            long duration = System.nanoTime() - start;
            Duration elapsed = ((Driver) jp.getTarget()).getDriverContext().getDriverStats().getElapsedTime();
            Duration blocked = ((Driver) jp.getTarget()).getDriverContext().getDriverStats().getTotalBlockedTime();

//            DriverContext driver = ((Driver) jp.getTarget()).getDriverContext();
//            Field field = jp.getTarget().getClass().getDeclaredField("overallTiming");
//            field.setAccessible(true);
//            OperationTimer.OperationTiming operatorTimer = (OperatorContext) field.get(jp.getTarget());
            System.out.println(Thread.currentThread().getName() + " Elasped time: " + elapsed + " Blocked time: " + blocked + " Duration: " + duration);
            return result;
        }

        if (name.contains("Finish")) {
            DateTime createTime = ((Driver) jp.getTarget()).getDriverContext().getDriverStats().getCreateTime();
            DateTime endTime = ((Driver) jp.getTarget()).getDriverContext().getDriverStats().getEndTime();
            if (endTime != null) {
                Period diff = new Period(createTime, endTime);
                System.out.println("Driver started @ " + createTime + " and ended @ " + endTime);
                System.out.println("Driver finished in :" + diff.getMillis() + "ms");
            }
        }
        return jp.proceed();
    }

    @Pointcut("call(* io.prestosql.operator.PipelineContext.*(..)) && target(io.prestosql.operator.PipelineContext)")
    public void pipelineContext() {
    }

    @Around("pipelineContext()")
    public Object aroundPipelineContext(ProceedingJoinPoint jp)
            throws Throwable {
        String name = jp.getSignature().getName();
//        if (name.contains("start")) {
//            long start = System.nanoTime();
////            System.out.println(Thread.currentThread().getName() + " started @ " + start);
//            // timer start
//            Object result = jp.proceed();
//            // result finished, record time
//            long duration = System.nanoTime() - start;
////            System.out.println(Thread.currentThread().getName() + " Duration: " + duration);
//            return result;
//        }

//        if (name.contains("Done")) {
//            System.out.println(Thread.currentThread().getName() + " pipeline finished @ " + System.nanoTime());
//        }
        return jp.proceed();
    }


    @Pointcut("call(* io.prestosql.operator.Operator.*(..)) && target(io.prestosql.operator.Operator)")
    public void operator() {
    }

    @Around("operator()")
    public Object around(ProceedingJoinPoint jp)
            throws Throwable {
        //injectLatency();
//        QueryStat queryStat =  QueryStat.get(queryId);
//
//        OpStat operator = queryStat.get(queryId).getOpstats().getOrDefault(Thread.currentThread().getName(), new OpStat());
//        operator.setName(Thread.currentThread().getName());
        String name = jp.getSignature().getName();
        if (name.contains("isFinished")) {
            String queryId = ((Operator) jp.getTarget()).getOperatorContext().getSession().getQueryId().getId();
            String operatorType = ((Operator) jp.getTarget()).getOperatorContext().getOperatorType();
            long outputWall = ((Operator) jp.getTarget()).getOperatorContext().getOperatorStats().getGetOutputWall().toMillis();
            long addInputWall = ((Operator) jp.getTarget()).getOperatorContext().getOperatorStats().getAddInputWall().toMillis();
            long finishWall = ((Operator) jp.getTarget()).getOperatorContext().getOperatorStats().getFinishWall().toMillis();
            long blockedWall = ((Operator) jp.getTarget()).getOperatorContext().getOperatorStats().getBlockedWall().toMillis();
            long totalWall = outputWall + addInputWall + finishWall + blockedWall;

            long time = System.nanoTime();
//            operator.setEnd(time);
            System.out.println(Thread.currentThread().getName() + "   ;  " + operatorType + "  ;  getOutputWall: " + outputWall + "  ;  addInputWall: " + addInputWall
                +  " ;  blockedWall: " + blockedWall + " ;  finishWall: " + finishWall + " ; totalWall: " + totalWall);
        }

//        queryStat.addStat(operator);

        if (name.contains("addInput") || name.contains("getOutput")) {
            long start = System.nanoTime();

            // timer start
            Object result = jp.proceed();
            // result finished, record time
            long duration = System.nanoTime() - start;
//            operator.addDuration(duration);

            //stage info, pipeline info, driver info, host
            String queryId = ((Operator) jp.getTarget()).getOperatorContext().getSession().getQueryId().getId();
            String operatorType = ((Operator) jp.getTarget()).getOperatorContext().getOperatorType();

            int pipelineId = ((Operator) jp.getTarget()).getOperatorContext().getDriverContext().getPipelineContext().getPipelineId();
            int taskId = ((Operator) jp.getTarget()).getOperatorContext().getDriverContext().getTaskId().getId();
            int stageId = ((Operator) jp.getTarget()).getOperatorContext().getOperatorStats().getStageId();
            Duration blockedWall = ((Operator) jp.getTarget()).getOperatorContext().getOperatorStats().getBlockedWall().convertTo(TimeUnit.NANOSECONDS);
            Duration finishWall = ((Operator) jp.getTarget()).getOperatorContext().getOperatorStats().getFinishWall().convertTo(TimeUnit.NANOSECONDS);

            if (blockedWall.convertToMostSuccinctTimeUnit().getValue() > 0 || finishWall.convertToMostSuccinctTimeUnit().getValue() > 0) {
                System.out.println(Thread.currentThread().getName()  + " / blocked ns: " + blockedWall.toMillis() + " / finish ns: " + finishWall.toMillis());
            }

            String[] fqcn = jp.getTarget().getClass().getName().split("\\.");
            String operatorInfo = Thread.currentThread().getName() + "/" + start + "_" + operatorType + "_" + name + "," + start + "," + duration;
            System.out.println(operatorInfo);
            writeToFile(operatorInfo, true);
//            queryStat.addStat(operator);
            return result;
        }
        return jp.proceed();
    }
}
