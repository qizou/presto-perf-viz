package io.perf.li;

import io.prestosql.operator.Operator;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * a tool that would allow capturing specific type of operators based on name
 * <p>
 * a map keep track of the time spent on each operator, each stage, each pipeline.
 * each invocation of a method will be kept track off, each operator will have a
 * - earliest start time
 * - each invocation start time
 * - each invocation end time
 * - latest end time;
 * all operators are link to the same query id
 */
@Aspect
public class PerformanceVisualization {

    Double blockTimeThreshold = 0.7;

    String relativePath = System.getProperty("user.dir");

    String propertiesFileName = "output-resource.properties";

    ConcurrentHashMap<String, String> operatorColors = new ConcurrentHashMap<String, String>();

    List<String> operatorTypes;

    ConcurrentHashMap<String, OperatorStats> operatorPerformance = new ConcurrentHashMap<String, OperatorStats>();

    ConcurrentHashMap<String, ConcurrentHashMap<String, Long>> pipelinePerformance = new ConcurrentHashMap<String, ConcurrentHashMap<String, Long>>();

    ArrayList<String> relatedOperators = new ArrayList<>();

    String queryId;


    @Pointcut("call(* io.prestosql.server.PrestoServer.*(..)) && target(io.prestosql.server.PrestoServer)")
    public void serverStart() {
    }

    @Around("serverStart()")
    public Object aroundStart(ProceedingJoinPoint jp)
            throws Throwable {
        String name = jp.getSignature().getName();
        if (name.contains("run")) {
            System.out.println("  ========================  AOP SERVER STARTING ====================");
            Thread.sleep(4000);
            Object result = jp.proceed();
            return result;
        }
        return jp.proceed();
    }

    @Pointcut("call(* io.prestosql.operator.Operator.*(..)) && target(io.prestosql.operator.Operator)")
    public void operator() {
    }

    @Around("operator()")
    public Object around(ProceedingJoinPoint jp)
            throws Throwable {
        String name = jp.getSignature().getName();

        if (name.contains("isFinished")) {

            String threadName = Thread.currentThread().getName();
            String pipelineId = threadName.substring(0, threadName.indexOf("-"));

            int queryIndex = pipelineId.indexOf(".");
            queryId = pipelineId.substring(0, queryIndex);

            String driverID = pipelineId.substring(queryIndex + 1);

            String operatorType = ((Operator) jp.getTarget()).getOperatorContext().getOperatorType();
            String extractedId = threadName.substring(queryIndex + 1);
            String targetOperator = extractedId + "_" + operatorType;

            long start = System.currentTimeMillis();

            Long outputWall = ((Operator) jp.getTarget()).getOperatorContext().getOperatorStats().getGetOutputWall().roundTo(TimeUnit.MILLISECONDS);
            Long addInputWall = ((Operator) jp.getTarget()).getOperatorContext().getOperatorStats().getAddInputWall().roundTo(TimeUnit.MILLISECONDS);
            Long blockedWall = ((Operator) jp.getTarget()).getOperatorContext().getOperatorStats().getBlockedWall().roundTo(TimeUnit.MILLISECONDS);
            Long finishWall = ((Operator) jp.getTarget()).getOperatorContext().getOperatorStats().getFinishWall().roundTo(TimeUnit.MILLISECONDS);
            Long totalWall = outputWall + addInputWall + finishWall + blockedWall;


            processPipelinePerformance(driverID, operatorType, blockedWall, totalWall);
            processOperatorPerformance(targetOperator, start, outputWall, addInputWall, blockedWall, finishWall, totalWall);
        }

        return jp.proceed();
    }


    @Pointcut("call(* io.prestosql.execution.QueryManagerStats.queryFinished(..)) && target(io.prestosql.execution.QueryManagerStats)")
    public void queryend() {
    }

    @Around("queryend()")
    public Object generateOutputs(ProceedingJoinPoint jp) throws Throwable {
        if (!operatorPerformance.isEmpty()) {
            initializeOperatorColors(operatorColors);
            countRelatedOperators(relatedOperators);

            String outputDirectory = getFileNameFromProperties(propertiesFileName, "directoryName");
            String querySummaryTxt = getFileNameFromProperties(propertiesFileName, "querySummaryTxt");
            String pipelinePercentagesTxt = getFileNameFromProperties(propertiesFileName, "pipelinePercentagesTxt");
            String pipelinePerformanceCSV = getFileNameFromProperties(propertiesFileName, "pipelinePerformanceCSV");
            String operatorColorCSV = getFileNameFromProperties(propertiesFileName, "operatorColorCSV");

            String filePath = relativePath + outputDirectory;
            createOutputDirectory(filePath);

            writeTxt(filePath, generateQueryInformation(), querySummaryTxt);
            writeTxt(filePath, generatePipelinePercentages(), pipelinePercentagesTxt);

            createPipelinePerformanceCSV(filePath, pipelinePerformanceCSV, pipelinePerformance);
            createOperatorColorCSV(filePath, operatorColorCSV, operatorColors);
        }

        return jp.proceed();
    }


    @Pointcut("call(* io.prestosql.execution.QueryManagerStats.queryStarted(..)) && target(io.prestosql.execution.QueryManagerStats)")
    public void queryStart() {
    }

    @Around("queryStart()")
    public Object statsCleanUp(ProceedingJoinPoint jp) throws Throwable {
        if (!operatorPerformance.isEmpty()) {
            operatorPerformance.clear();
            pipelinePerformance.clear();
            relatedOperators.clear();
        }

        return jp.proceed();
    }

    public String readFromProperties(String propertiesFileName, String fieldName) {
        String result = "";
        BufferedReader bufferedReader = null;
        try {
            String relativePath = System.getProperty("user.dir");
            String propFilePath = relativePath + "/" + propertiesFileName;

            Properties properties = new Properties();

            bufferedReader = new BufferedReader(new FileReader(propFilePath));
            properties.load(bufferedReader);
            result = properties.getProperty(fieldName);
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            try{
                if(bufferedReader != null){
                    bufferedReader.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return result;
    }


    public String getFileNameFromProperties(String propertiesFileName, String fieldName) {
        return readFromProperties(propertiesFileName, fieldName);
    }

    public void countRelatedOperators(ArrayList<String> relatedOperators) {
        ArrayList<String> sortedPipelineIds = sortPipelineByDuration();
        for (String d : sortedPipelineIds) {
            ConcurrentHashMap<String, Long> targetPipelineInfo = pipelinePerformance.get(d);
            for (String op : operatorTypes) {
                if (targetPipelineInfo.containsKey(op) && (!relatedOperators.contains(op))) {
                    relatedOperators.add(op);
                }
            }
        }
    }

    public StringBuilder generateQueryInformation() {
        StringBuilder result = new StringBuilder();

        result.append("Query id:");
        result.append(queryId);
        result.append("\r\n");
        return result;
    }

    public StringBuilder generatePipelinePercentages() {
        ArrayList<String> sortedPipelineIds = sortPipelineByDuration();

        StringBuilder result = new StringBuilder();

        for (String pipelineId : sortedPipelineIds) {
            result.append(pipelineId);
            result.append(": ");
            ConcurrentHashMap<String, Long> targetPipelineInfo = pipelinePerformance.get(pipelineId);
            Long totalDuration = 0L;
            if (targetPipelineInfo != null) {
                for (String op : operatorTypes) {
                    if (pipelinePerformance.get(pipelineId).containsKey(op)) {
                        totalDuration += pipelinePerformance.get(pipelineId).get(op);
                    }
                }
                for (String op : operatorTypes) {
                    if (targetPipelineInfo.containsKey(op) && targetPipelineInfo.get(op) != null) {
                        double myDuration = (double) (targetPipelineInfo.get(op)) * 100;
                        double temp = myDuration / totalDuration;
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        result.append(op).append("=").append(decimalFormat.format(temp)).append(",");
                    }
                }
                result.append("\r\n");
            }
        }
        result.append("\r\n");
        return result;
    }

    public void createOperatorColorCSV(String filePath, String fileName, ConcurrentHashMap<String, String> operatorColors) {

        Object[] head = relatedOperators.toArray();
        List<Object> headList = Arrays.asList(head);

        List<List<Object>> dataList = new ArrayList<List<Object>>();
        List<Object> rowList = null;

        rowList = new ArrayList<Object>();
        for (String op : relatedOperators) {
            if (op != null && operatorColors.containsKey(op)) {
                rowList.add(operatorColors.get(op));
            }
        }
        dataList.add(rowList);

        writeCSV(filePath, fileName, headList, dataList);

    }

    public void createPipelinePerformanceCSV(String filePath, String fileName, ConcurrentHashMap<String, ConcurrentHashMap<String, Long>> pipelinePerformance) {

        ArrayList<String> sortedPipelineIds = sortPipelineByDuration();
        List<List<Object>> dataList = new ArrayList<List<Object>>();
        List<Object> rowList = null;

        for (String d : sortedPipelineIds) {
            rowList = new ArrayList<Object>();
            rowList.add(d);
            ConcurrentHashMap<String, Long> targetPipelineInfo = pipelinePerformance.get(d);
            ArrayList<String> valueOperator = new ArrayList<>(targetPipelineInfo.keySet());
            for (String op : relatedOperators) {
                if (valueOperator.contains(op)) {
                    Long duration = targetPipelineInfo.get(op);
                    rowList.add(duration);
                } else {
                    rowList.add(0);
                }
            }
            dataList.add(rowList);
        }

        List<Object> headList = new ArrayList<Object>();
        headList.add(0, "Pipeline");
        headList.addAll(relatedOperators);

        writeCSV(filePath, fileName, headList, dataList);
    }

    public void writeCSV(String filePath, String fileName, List<Object> headList, List<List<Object>> dataList) {
        File csvFile = null;
        BufferedWriter csvWriter = null;
        try {
            csvFile = new File(filePath + fileName);

            //delete
            csvFile.createNewFile();

            csvWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile), StandardCharsets.UTF_8), 1024);

            int num = headList.size() / 2;
            StringBuffer buffer = new StringBuffer();
            for (int i = 0; i < num; i++) {
                buffer.append(" ,");
            }

            // write head
            writeRow(headList, csvWriter);

            // write data
            for (List<Object> row : dataList) {
                writeRow(row, csvWriter);
            }

            csvWriter.flush();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            try {
                csvWriter.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void writeTxt(String filePath, StringBuilder result, String filename) {
        BufferedWriter out = null;
        try {
            File writeFileName = new File(filePath + filename);
            writeFileName.createNewFile();
            out = new BufferedWriter(new FileWriter(writeFileName));
            out.write(String.valueOf(result));
            out.flush();

        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally{
            try{
                if (out != null){
                    out.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private static void writeRow(List<Object> row, BufferedWriter csvWriter) throws IOException {
        for (Object data : row) {
            StringBuffer stringBuffer = new StringBuffer();
            String rowStr = stringBuffer.append("\"").append(data).append("\",").toString();
            csvWriter.write(rowStr);
        }
        csvWriter.newLine();
    }

    public ArrayList<String> sortPipelineByDuration() {
        ArrayList<String> pipelineIds = new ArrayList<String>(pipelinePerformance.keySet());
        pipelineIds.sort(new Comparator<String>() {
            @Override
            public int compare(String pip1, String pip2) {
                ConcurrentHashMap<String, Long> targetPipelineInfo1 = pipelinePerformance.get(pip1);
                ConcurrentHashMap<String, Long> targetPipelineInfo2 = pipelinePerformance.get(pip2);
                Long totalDuration1 = 0L;
                Long totalDuration2 = 0L;
                for (String op : operatorTypes) {
                    if ((targetPipelineInfo1.containsKey(op)) && (targetPipelineInfo1.get(op) != null)) {
                        totalDuration1 += targetPipelineInfo1.get(op);
                    }
                    if (targetPipelineInfo2.containsKey(op) && (targetPipelineInfo2.get(op) != null)) {
                        totalDuration2 += targetPipelineInfo2.get(op);
                    }
                }

                if (totalDuration1 > totalDuration2) {
                    return -1;
                } else if (totalDuration1 < totalDuration2) {
                    return 1;
                }
                return 0;
            }
        });
        return pipelineIds;
    }

    public void processPipelinePerformance(String driverID, String operatorType, long blockedWall, long totalWall) {

        if (!pipelinePerformance.containsKey(driverID)) {
            ConcurrentHashMap<String, Long> targetPipelineInfo = new ConcurrentHashMap<String, Long>();
            targetPipelineInfo.put(operatorType, totalWall);
            pipelinePerformance.put(driverID, targetPipelineInfo);
        } else {
            ConcurrentHashMap<String, Long> targetPipelineInfo = pipelinePerformance.get(driverID);
            if (blockedWall < totalWall * blockTimeThreshold) {
                if (!targetPipelineInfo.containsKey(operatorType)) {
                    pipelinePerformance.get(driverID).put(operatorType, totalWall);
                } else {
                    long oldDuration = targetPipelineInfo.get(operatorType);
                    long newDuration = oldDuration + totalWall;
                    pipelinePerformance.get(driverID).put(operatorType, newDuration);
                }
            }
        }
    }

    public void processOperatorPerformance(String targetOperator, long start, long outputWall, long addInputWall, long blockedWall, long finishWall, long totalWall) {
        if (!operatorPerformance.containsKey(targetOperator)) {
            OperatorStats operatorStats = new OperatorStats(start, outputWall, addInputWall, blockedWall, finishWall, totalWall);
            operatorPerformance.put(targetOperator, operatorStats);
        } else {
            OperatorStats targetOperatorStats = operatorPerformance.get(targetOperator);

            long oldOutputWall = targetOperatorStats.getOutputWall();
            targetOperatorStats.setOutputWall(oldOutputWall + outputWall);

            long oldAddInputWall = targetOperatorStats.getAddInputWall();
            targetOperatorStats.setAddInputWall(oldAddInputWall + addInputWall);

            long oldBlockedWall = targetOperatorStats.getBlockedWall();
            targetOperatorStats.setBlockedWall(oldBlockedWall + blockedWall);

            long oldFinishWall = targetOperatorStats.getFinishWall();
            targetOperatorStats.setFinishWall(oldFinishWall + finishWall);

            long oldTotalWall = targetOperatorStats.getTotalWall();
            targetOperatorStats.setTotalWall(oldTotalWall + totalWall);
        }
    }

    public void initializeOperatorColors(ConcurrentHashMap<String, String> operatorColors) {
        operatorColors.put("TableScanOperator", "#b71c1c");
        operatorColors.put("ScanFilterAndProjectOperator", "#880e4f");
        operatorColors.put("LocalExchangeSinkOperator", "#DB39B3");
        operatorColors.put("HashBuilderOperator", "#00BCD4");
        operatorColors.put("LocalExchangeSourceOperator", "#ABEBC6");
        operatorColors.put("FilterAndProjectOperator", "#f78181");
        operatorColors.put("LookupJoinOperator", "#66bb6a");
        operatorColors.put("AggregationOperator", "#1b5e20");
        operatorColors.put("TaskOutputOperator", "#FF8F00");
        operatorColors.put("TopNOperator", "#9825b8");
        operatorColors.put("ExchangeOperator", "#d279a6");
        operatorColors.put("DynamicFilterOperator", " #F57F17");
        operatorColors.put("AssignUniqueIdOperator", "#E65100");
        operatorColors.put("HashAggregationOperator", "#CE93D8");
        operatorColors.put("MarkDistinctOperator", "#C2185B");
        operatorColors.put("PartitionedOutputOperator", "#FDD835");
        operatorColors.put("WindowOperator", "#512DA8 ");
        operatorColors.put("OrderByOperator", "#303F9F");
        operatorColors.put("LocalMergeSourceOperator", "#1976D2 ");
        operatorColors.put("SetBuilderOperator", "#0097A7");
        operatorColors.put("HashSemiJoinOperator", "#00796B ");
        operatorColors.put("NestedLoopBuildOperator", "#388E3C");
        operatorColors.put("NestedLoopJoinOperator", "#689F38 ");
        operatorColors.put("GroupIdOperator", " #AFB42B");
        operatorColors.put("EnforceSingleRowOperator", "#FBC02D");
        operatorColors.put("MergeOperator", "#006064");
        operatorColors.put("BlockedOperator", "#F57C00 ");
        operatorColors.put("BrokenOperator", " #E64A19");
        operatorColors.put("DeleteOperator", "#E57373 ");
        operatorColors.put("DevNullOperator", "#F06292");
        operatorColors.put("DistinctLimitOperator", "#BA68C8");
        operatorColors.put("ExplainAnalyzeOperator", "#9575CD");
        operatorColors.put("FinishedOperator", "#7986CB ");
        operatorColors.put("IndexSourceOperator", "#64B5F6 ");
        operatorColors.put("LimitOperator", "#4FC3F7");
        operatorColors.put("NotBlockedTableScanOperator", "#4DD0E1 ");
        operatorColors.put("NullOutputOperator", "#4DB6AC");
        operatorColors.put("PageBufferOperator", "#81C784");
        operatorColors.put("PageConsumerOperator", "#AED581");
        operatorColors.put("PageSourceOperator", "#AED581");
        operatorColors.put("PagesIndexBuilderOperator", "#DCE775");
        operatorColors.put("RowNumberOperator", "#FFF176 ");
        operatorColors.put("SpatialIndexBuilderOperator", "#FFB74D");
        operatorColors.put("SpatialJoinOperator", "#FF8A65");
        operatorColors.put("StatisticsWriterOperator", "#BCAAA4");
        operatorColors.put("StreamingAggregationOperator", "#EF9A9A");
        operatorColors.put("TableDeleteOperator", "#F48FB1");
        operatorColors.put("TableFinishOperator", "#42A5F5");
        operatorColors.put("TableWriterOperator", "#9FA8DA");
        operatorColors.put("TopNRowNumberOperator", "#64B5F6");
        operatorColors.put("VacuumTableOperator", "#80DEEA");
        operatorColors.put("ValuesOperator", "#A5D6A7");
        operatorColors.put("WorkProcessorOperatorAdapter", "#FFF59D");
        operatorColors.put("WorkProcessorPipelineSourceOperator", "#FFE082");
        operatorColors.put("WorkProcessorSourceOperatorAdapter", "#FFCCBC");

        operatorTypes = new ArrayList<>(operatorColors.keySet());
    }

    public void createOutputDirectory(String filePath) {
        File directoryName = new File(filePath);
        if (!directoryName.exists()) {
            directoryName.mkdirs();
        }
    }

    class OperatorStats{
        long start;
        long outputWall;
        long addInputWall;
        long blockedWall;
        long finishWall;
        long totalWall;

        OperatorStats(long start, long outputWall, long addInputWall, long blockedWall, long finishWall, long totalWall) {
            this.start = start;
            this.outputWall = outputWall;
            this.addInputWall = addInputWall;
            this.blockedWall = blockedWall;
            this.finishWall = finishWall;
            this.totalWall = totalWall;
        }

        long getStart(){return this.start;}

        long getOutputWall(){ return this.outputWall;}

        long getAddInputWall(){return this.addInputWall;};

        long getBlockedWall(){return this.blockedWall;}

        long getFinishWall(){return this.finishWall;}

        long getTotalWall(){ return this.totalWall;}

        void setOutputWall(long newOutputWall) {this.outputWall = newOutputWall;}

        void setAddInputWall(long newAddInputWall) {this.addInputWall = newAddInputWall;}

        void setBlockedWall(long newBlockedWall) {this.blockedWall = newBlockedWall;}

        void setFinishWall(long newFinishWall){this.finishWall = newFinishWall;}

        void setTotalWall(long newTotalWall){this.totalWall = newTotalWall;}
    }

}


