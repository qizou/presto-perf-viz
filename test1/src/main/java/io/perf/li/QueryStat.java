package io.perf.li;

import java.util.HashMap;

public class QueryStat {
    public HashMap<Object, OpStat> opstats;

    public static HashMap<String, QueryStat> stats = new HashMap<String, QueryStat>();

    public QueryStat() {
        if (this.opstats == null) {
            this.opstats = new HashMap<>();
        }
    }

    public static QueryStat get(String queryId) {
        new QueryStat();
        return stats.get(queryId);
    }

    public void addStat(OpStat stat) {
        this.opstats.put(stat.getName(), stat);
    }

    public HashMap<Object, OpStat> getOpstats() {
        if (this.opstats == null) {
            opstats = new HashMap<>();
        }
        return this.opstats;
    }
}
