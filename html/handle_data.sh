#!/bin/sh -exu

FILEPATH='var/www/html/output-files'
FILENAME='pipeline-performance'

sed -i 's/\"//g' /${FILEPATH}/${FILENAME} | sed -i 's/\(.*\),\(.*\)/\1\2/' /${FILEPATH}/${FILENAME}

python calculate_op_percentages.py

exit