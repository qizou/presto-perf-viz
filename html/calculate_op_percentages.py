import pandas as pd
from decimal import Decimal

path='output-files/'

new_file=path+ 'pip-combine3.csv'

file1=path+"pip-combine.csv"
df = pd.read_csv(file1)

with open(file1,"r") as f:
  first_line=f.readline()
  first_line = first_line.strip("\r\n")

operators = first_line.split(",")

total_sum = 0 
sums = []
new_operators = []
for i in range(1, len(operators)):
  op_sum = df[operators[i]].sum()
  sums.append(op_sum)
  total_sum += int(op_sum)

#calculate percentages
new_operators.append("Driver")
for i in range(1, len(operators)):
  x=Decimal(float(sums[i-1] * 100)/total_sum)
  
  op_percentage = round(x,2)
  temp_str = operators[i] + " " + str(op_percentage)+"%"
  new_operators.append(temp_str)
  
#handle new file
new_op_str = ','.join(new_operators)
with open(new_file, "w") as nf:
  with open(file1, 'r') as f:
      lines=f.readlines()[1:]
      nf.write(new_op_str + "\n")
      nf.writelines(lines)