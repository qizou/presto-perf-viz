import pandas as pd

pd.set_option('display.max_columns', 1000)

path="/nodes-output/"
#csv path
path0 = path + "temp-pipeline-csv1/"
path2 = path + "temp-pipeline-csv2/"

#txt path
path4 = path + "temp-pipeline-txt/"

servers = []
workers = []

#handle operator order problem
for s in servers:
    df = pd.read_csv( path0 + "pipeline-performance-" + s + ".csv")

    #identify worker node
    df["Pipeline"] = df["Pipeline"].astype(str)
    temp = "--" + df.at[0, "Pipeline"]
    df.at[0,"Pipeline"] = s + temp

    df.drop(df.filter(reges="Unname"), axis=1, inplace=True)

    #sort operators
    data = df.sort_index(axis=1)
    data=data.drop_duplicates()
    data.to_csv(path2 + "pip" + s + ".csv", index=False)

#combine csv
#replace server in "pipserver.csv" with real server name
df1 = pd.read_csv(path2 + "pipserver.csv")

for w in workers:
    df2 = pd.read_csv(path2 + "pip" + w + ".csv")
    df1 = pd.concat([df1,df2], ignore_index=True, sort=False)
    df1.drop(df.filter(reges="Unname"), axis=1, inplace=True)

df1=df1.fillna(0)
df1=df1.drop_duplicates()
df1.to_csv(path + "pipeline-combined.csv", index=False, sep=",")

#txt files
combine_file = path + "pipeline-percentages-combined.txt"
with open(combine_file, 'w') as m:
    for s in servers:
        file = path4 + "pip-" + s + ".txt"
        with open(file, 'r') as f:
            lines = f.readlines()
            m.write(s + "--")
            m.writelines(lines)