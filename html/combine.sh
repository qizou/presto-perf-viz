#!/bin/sh -exu

VERSION='/nodes-output'

servers = ()

wait

rm -rf /${VERSION}/temp-pipeline-csv1/

mkdir /${VERSION}/temp-pipeline-csv1/

rm -rf /${VERSION}/temp-pipeline-csv2/

mkdir /${VERSION}/temp-pipeline-csv2/

rm -rf /${VERSION}/temp-pipeline-txt/

mkdir /${VERSION}/temp-pipeline-txt/

for i in ${servers[@]}; do echo; echo '> '$i; cp /${VERSION}/${i}/output-files/pipeline-performance.csv /${VERSION}/temp-pipeline-csv1/pipeline-performance-${i}.csv & done

for i in ${servers[@]}; do echo; echo '> '$i; cp /${VERSION}/${i}/output-files/pipeline-percentages.txt /${VERSION}/temp-pipeline-txt/pip-${i}.txt & done

cp /${VERSION}/servernode/output-files/total-summary.txt /${VERSION}/total.txt

cp /${VERSION}/servernode/output-files/operator-color.csv /${VERSION}/color.csv

wait

python combine.py

rm -rf /${VERSION}/temp-pipeline-csv1/

rm -rf /${VERSION}/temp-pipeline-csv2/

rm -rf /${VERSION}/temp-pipeline-txt/

exit